# Calibrat3D
This app is the final project for the Udacity course 
'_Android Development for Beginners_'.  
Its intent is to aid owners of 3d printers in the process of calibrating their machine, give some insights on the various aspects of 3d printing and some general tips for better results.

## Prerequisites
To run __Calibrat3d__ on your device you need at least _Android 4.0.4 Ice Cream Sandwich_ but that has not been tested.  
The app runs happily on _Android 4.4.2 KitKat_ and _Android 5.1.1 Lollipop_ tho.
## Installation
Download the `app-debug.apk` to your device.  
On your device browse to the folder you downloaded the `.apk` to and run it.  

__Calibrat3d__ should now be installed on your device.  
Browse your home menu untill you find its icon and tap on it.
***
## Source project import in Android Studio 1.5 for Windows
### Prerequisites
* Android Studio 1.5
* Java 1.8.0_73
* SDK 15
* ADB drivers for your device
### Procedure
Start by downloading and unzipping this repository to your favourite storage medium or clone it.  
Open Android Studio.  
In the _Quick Start_ menu choose "_Open an existing Android Studio project_"  
Browse to the folder you put the repository in, you should see the AS green icon near the _MultiAct_ folder.  
Select the _MultiAct_ folder and click OK.  
The project should now open.  
>_The project repository is of course under version control, if a popup asks you to register its root you can ignore it or register the root and keep on working._
### Run it!
To upload and run __Calibrat3d__ to your device make sure its connected and recognised, then simply press the green arrow in the tool bar or use the shortcut _shift+F10_.  
Alternatively click on the _Run_ menu and then on _Run 'app'_. 

## Eclipse (all platforms)
Sorry, you're out of luck. I don't use Eclipse for android development so I have no direct experience but the internet says:  

    You cannot import the project directly but it is not too hard to achieve it:  

    * Create a new Android empty project in eclipse  
    * Overwrite the fresh res/ folder and the AndroidManifest.xml file, with the ones from the Android Studio project  
    * Copy the content of the java/ folder from the Android Studio project in the Eclipse src folder
    
Basically the Android application fundamental elements are the java files, the manifest file and the resources.   
From there you can build back a project in your favorite IDE
***
## Usage
Open the app, you'll be presented with a menu. You can then choose the area you want to learn more about and/or you want to calibrate on your printer.  
A page will open containing a brief explanation and a summary of the processes involved in calibrating that particular aspect of your printer.  
If applicable you'll find some handy tools to aid with calculations and decisions on how to proceed and what to do.

Enjoy!
## License
* All my code is licensed under the strict WTFPL license (relevant excerpts at http://www.wtfpl.net/about/)
* Images are used without permission so they probably shouldn't be here. 
* Almost all texts are copied verbatim from Triffid Hunter's calibration guide which is available under GNU Free Documentation License 1.2 (http://www.gnu.org/copyleft/fdl.html)