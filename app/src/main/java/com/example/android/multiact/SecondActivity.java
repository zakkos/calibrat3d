/*
Calibrat3d - Final project for the Udacity course Android Development for Beginners
Submitted for grading by Valerio Sacchetto
 */
package com.example.android.multiact;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        String menu_id = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        contentLoader(menu_id);
    }

    /**
     * This function parses the user menu input and sets the correct content layout
     *
     * @param menu_id_string is the index of the main menu which the user selected
     */
    private void contentLoader(String menu_id_string) {
        int menu_id = Integer.parseInt(menu_id_string);
        switch (menu_id) {
            case 0:
                setContentView(R.layout.activity_motors);
                setTitle(R.string.title_01);
                break;
            case 1:
                setContentView(R.layout.activity_extrusion);
                setTitle(R.string.title_02);
                break;
            case 2:
                setContentView(R.layout.activity_height);
                setTitle(R.string.title_03);
                break;
            case 3:
                setContentView(R.layout.activity_temperatures);
                setTitle(R.string.title_04);
                break;
            case 4:
                setContentView(R.layout.activity_costs);
                setTitle(R.string.title_05);
                break;

            default:
                setContentView(R.layout.activity_motors);
                setTitle(R.string.title_01);
                break;
        }
    }

    /*
     * This section contains all the functions pertaining the first screen
     */

    /**
     * This function retrieves the values to compute the steps per mm and then displays what number
     * the user has to put into the printer firmware in the motors calibration section.
     *
     * The calculation is:
     * stepsPerMM = (microsteps * stepsPerRevolution) / (beltPitch * pulleyTeeth)
     */
    public void calculateStepsMM(View view) {
        TextView stepsMm_display = (TextView) findViewById(R.id.stepsMM_txtView);
        float stepsPerMm = (getMicroSteps(R.id.microstepSpinner) * getMotorSteps(R.id.motorStepsSpinner)) / (getEditTextFloat(R.id.beltPitch_editTxt, R.string.pitchDefault) * getEditTextInt(R.id.pulley_editTxt, R.string.pulleyDefault));
        stepsMm_display.setText(Float.toString(stepsPerMm));
    }

    /**
     * This function retrieves the values to compute the steps per mm and then displays what number
     * the user has to put into the printer firmware in the Z-motors calibration section.
     *
     * The calculation is:
     * stepsPerMM = (microsteps * stepsPerRevolution) / threadPitch
     */
    public void calculateThreadStepsMM(View view) {
        TextView stepsMm_display = (TextView) findViewById(R.id.zStepsMM_txtView);
        float stepsPerMm = (getMicroSteps(R.id.zMicrostepSpinner) * getMotorSteps(R.id.zMotorStepsSpinner)) / getEditTextFloat(R.id.threadPitch_editTxt, R.string.threadDefault);
        stepsMm_display.setText(Float.toString(stepsPerMm));
    }

    /*
     * End of first section
     */

    /*
     * This section contains all the functions pertaining the second screen
     */

    /**
     * This function checks whether the user wants to use a geared extruder or not and enables or
     * disables the input texts accordingly.
     */
    public void enableGeared (View view) {
        CheckBox gearedChk = (CheckBox) findViewById(R.id.gearCheck);
        EditText smallEditTxt = (EditText) findViewById(R.id.smallGear_editTxt);
        EditText bigEditTxt = (EditText) findViewById(R.id.bigGear_editTxt);

        if(gearedChk.isChecked()) {
            smallEditTxt.setEnabled(true);
            bigEditTxt.setEnabled(true);
        } else {
            smallEditTxt.setEnabled(false);
            bigEditTxt.setEnabled(false);
        }
    }

    /**
     * This function retrieves the values to compute the steps per mm and then displays what number
     * the user has to put into the printer firmware in the extruder calibration section.
     *
     * The calculations are:
     * gearRatio = bigGear / smallGear (if the extruder is NOT geared than the ratio is 1)
     * stepsPerMM = (microsteps * stepsPerRevolution) * gearRatio / (hobDiameter * 3.14159)
     */
    public void calculateEStepsMM(View view) {
        TextView stepsMm_display = (TextView) findViewById(R.id.stepsMM_txtView);
        CheckBox gearedChk = (CheckBox) findViewById(R.id.gearCheck);
        float gearRatio;

        if(gearedChk.isChecked()) {
            gearRatio = (getEditTextFloat(R.id.bigGear_editTxt, R.string.bigGearDefault) / getEditTextFloat(R.id.smallGear_editTxt, R.string.smallGearDefault));
        } else {
            gearRatio = 1.0F;
        }

        float stepsPerMm = (getMicroSteps(R.id.microstepSpinner) * getMotorSteps(R.id.motorStepsSpinner)) * gearRatio / (getEditTextFloat(R.id.hob_editTxt,R.string.hobDefault)*3.14159F);
        stepsMm_display.setText(Float.toString(stepsPerMm));
    }

    /**
     * This function retrieves the values to compute the fine tuned steps per mm and then displays what number
     * the user has to put into the printer firmware in the extruder calibration section.
     * The user has to extrude 100mm of filament and then measure how much filament was actually
     * extruded. By comparing the measurements we can get to the correct value of steps per mm.
     *
     * The calculation is:
     * stepsPerMM = oldESteps * (100 / measuredExtrudedFilament)
     */
    public void calculateAdjustedEStepsMM(View view) {
        TextView stepsMm_display = (TextView) findViewById(R.id.fStepsMM_txtView);
        float stepsPerMm = getEditTextFloat(R.id.oldESteps_editTxt, R.string.eStepsDefault)*(100F/getEditTextFloat(R.id.actualLength_editTxt, R.string.lengthDefault));
        stepsMm_display.setText(Float.toString(stepsPerMm));
    }

    /*
     * End of second section
     */

    /*
     * This section contains all the functions pertaining the third screen
     */

    /**
     * This function retrieves the values to compute the maximum layer height and then displays what number
     * the user has to put into the slicer software for layer height.
     * To get better prints the layer height should not exceed 80% of the nozzle size.
     *
     * The calculation is:
     * maxLayerHeight = nozzleSize * 0.8
     */
    public void calculateLayerHeight (View view) {
        TextView layer_display = (TextView) findViewById(R.id.layerHeight_txtView);
        float layer_height = getEditTextFloat(R.id.nozzleDiameter_editTxt, R.string.nozzleDefault) * 0.8F;
        layer_display.setText(Float.toString(layer_height));
    }
    /*
     * End of third section
     */

    /*
     * This section contains all the functions pertaining the fifth screen
     */

    /**
     * This function retrieves the values to compute the total printing cost and then displays it.
     * To get the total cost of a print we have to sum the cost of the material printed and the
     * cost of the printing process (energy consumed, operator pay, machine wear etc).
     *
     * The material cost is calculated with this formula:
     * materialCost = (filamentLengthUsed * materialDensity * volumeUnit) * costPerKg
     *
     * The process cost is calculated with this formula:
     * processCost = hourlyRate * hours
     */
    public void calculateCost (View view) {
        TextView cost_display = (TextView) findViewById(R.id.cost_txtView);
        double cost = (getEditTextFloat(R.id.spoolCost_editTxt, R.string.spoolCost_Default) * getEditTextFloat(R.id.objectLength_editTxt, R.string.objectLength_Default) * getVolumeUnit(R.id.filamentSizeSpinner) * getDensity(R.id.materialDensitySpinner))+(getEditTextFloat(R.id.jobLength_editTxt, R.string.jobLength_Default)*getEditTextFloat(R.id.timeCost_editTxt, R.string.timeCost_Default));
        cost_display.setText(Double.toString(cost));
    }
    /*
     * End of fifth section
     */

    /**
     * This function maps the spinner index selected by the user  to the value for which
     * a single step is divided by the motors driver.
     *
     * @param spinnerId is the id of the spinner we want to retrieve the value of
     * @return an int representing the denominator part of the fraction 1/microstepping value
     */
    private int getMicroSteps(int spinnerId) {
        Spinner microSpinner = (Spinner) findViewById(spinnerId);
        int microSpinnerIndex = microSpinner.getSelectedItemPosition();
        switch (microSpinnerIndex) {
            case 0:
                return 1;

            case 1:
                return 2;

            case 2:
                return 4;

            case 3:
                return 8;

            case 4:
                return 16;

            case 5:
                return 32;


            default:
                return 1;

        }
    }

    /**
     * This function maps the spinner index selected by the user
     * to the number of steps it takes the motor to make a full turn.
     *
     * @param spinnerId is the id of the spinner we want to retrieve the value of
     * @return an int representing the number of steps for a full turn of the motor
     */
    private int getMotorSteps(int spinnerId) {
        Spinner stepsSpinner = (Spinner) findViewById(spinnerId);
        int stepsSpinnerIndex = stepsSpinner.getSelectedItemPosition();
        switch (stepsSpinnerIndex) {
            case 0:
                return 200;

            case 1:
                return 400;

            case 2:
                return 48;


            default:
                return 200;

        }
    }

    /**
     * This function maps the spinner index selected by the user
     * to the value of material density expressed in g/cm^3
     *
     * @param spinnerId is the id of the spinner we want to retrieve the value of
     * @return a float representing the material density expressed in g/cm^3
     */
    private float getDensity(int spinnerId) {
        Spinner microSpinner = (Spinner) findViewById(spinnerId);
        int microSpinnerIndex = microSpinner.getSelectedItemPosition();
        switch (microSpinnerIndex) {
            case 0:
                return 1.03F;

            case 1:
                return 1.24F;

            default:
                return 1.03F;

        }
    }

    /**
     * This function maps the spinner index selected by the user
     * to the adjusted volume per unit based on the filament diameter
     * The calculations to get this number are:
     * Volume = (3.14159 * radius^2) * h
     * All units are in mm, h is for a cylinder of height 10. Volume is then converted to cm^3
     * to match density values that are expressed in g/cm^3
     *
     * @param spinnerId is the id of the spinner we want to retrieve the value of
     * @return a double representing the volume per unit
     */
    private double getVolumeUnit(int spinnerId) {
        Spinner stepsSpinner = (Spinner) findViewById(spinnerId);
        int stepsSpinnerIndex = stepsSpinner.getSelectedItemPosition();
        switch (stepsSpinnerIndex) {
            case 0:
                return 0.002405;

            case 1:
                return 0.007065;

            default:
                return 0.002405;

        }
    }

    /**
     * This function retrieves a user input float value,
     * checks for its validity and returns its value or a default one if the user input is not a
     * valid one
     *
     * @param editTxtId is the id of the EditText from which we want to extract the value
     * @param defaultValStr  is the resource id of a string that contains the default value for this EditText
     * @return a float representing the user input
     */
    private float getEditTextFloat(int editTxtId, int defaultValStr) {
        //get the EditText object and store its value in a string
        EditText editTxt = (EditText) findViewById(editTxtId);
        String editTxt_value = editTxt.getText().toString();

        //clear the previous error if present
        editTxt.setError(null);

        //check if the user input is invalid. If true return a default value and display an error,
        // else return the user input parsed as a float
        if (TextUtils.isEmpty(editTxt_value) || Float.parseFloat(editTxt_value) == 0F) {
            editTxt.setError(getString(R.string.emptyErr));
            editTxt.setText(defaultValStr);
            return Float.parseFloat(getString(defaultValStr));
        } else {
            return Float.parseFloat(editTxt_value);
        }
    }

    /**
     * This function retrieves the user input int value,
     * checks for its validity and returns its value or a default one if the user input is not a
     * valid one
     *
     * @param editTxtId is the id of the EditText from which we want to extract the value
     * @param defaultValStr  is the resource id of a string that contains the default value for this EditText
     * @return an int representing the number of teeth in the pulleys
     */
    private int getEditTextInt(int editTxtId, int defaultValStr) {
        //get the EditText object and store its value in a string
        EditText editTxt = (EditText) findViewById(editTxtId);
        String editTxt_value = editTxt.getText().toString();

        //clear the previous error if present
        editTxt.setError(null);

        //check if the user input is invalid. If true return a default value and display an error,
        // else return the user input parsed as an int
        if (TextUtils.isEmpty(editTxt_value) || Integer.parseInt(editTxt_value) == 0) {
            editTxt.setError(getString(R.string.emptyErr));
            editTxt.setText(defaultValStr);
            return Integer.parseInt(getString(defaultValStr));
        } else {
            return Integer.parseInt(editTxt_value);
        }
    }

}
