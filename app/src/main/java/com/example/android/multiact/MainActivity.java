/*
Calibrat3d - Final project for the Udacity course Android Development for Beginners
Submitted for grading by Valerio Sacchetto
 */
package com.example.android.multiact;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public final static String EXTRA_MESSAGE = "com.example.android.multiact.ACTIVITY_DISPLAY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RelativeLayout menu_01 = (RelativeLayout) findViewById(R.id.menu_01);
        RelativeLayout menu_02 = (RelativeLayout) findViewById(R.id.menu_02);
        RelativeLayout menu_03 = (RelativeLayout) findViewById(R.id.menu_03);
        RelativeLayout menu_04 = (RelativeLayout) findViewById(R.id.menu_04);
        RelativeLayout menu_05 = (RelativeLayout) findViewById(R.id.menu_05);
        menu_01.setClickable(true);
        menu_01.setOnClickListener(this);
        menu_02.setClickable(true);
        menu_02.setOnClickListener(this);
        menu_03.setClickable(true);
        menu_03.setOnClickListener(this);
        menu_04.setClickable(true);
        menu_04.setOnClickListener(this);
        menu_05.setClickable(true);
        menu_05.setOnClickListener(this);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_action_icon);
    }

    public void onClick(View v){
        int mID = v.getId();
        Intent switcher = new Intent(this, SecondActivity.class);
        switch (mID){
            case R.id.menu_01:
                switcher.putExtra(EXTRA_MESSAGE, "0");
                startActivity(switcher);
                break;
            case R.id.menu_02:
                switcher.putExtra(EXTRA_MESSAGE, "1");
                startActivity(switcher);
                break;
            case R.id.menu_03:
                switcher.putExtra(EXTRA_MESSAGE, "2");
                startActivity(switcher);
                break;
            case R.id.menu_04:
                switcher.putExtra(EXTRA_MESSAGE, "3");
                startActivity(switcher);
                break;
            case R.id.menu_05:
                switcher.putExtra(EXTRA_MESSAGE, "4");
                startActivity(switcher);
                break;

            default:
                switcher.putExtra(EXTRA_MESSAGE, "0");
                startActivity(switcher);
                break;
        }
    }
}

